import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Wisielec {
    static pomocnikWisielec pomocnik = new pomocnikWisielec();

    public static void main(String[] args) {
        int etap = pomocnik.getEtap();
        pomocnik.przywitanie();

        pomocnik.setLosoweSlowo();


        pomocnik.szyfrowanie();
        System.out.println(pomocnik.getZaszyfrowaneSlowo());

        while (etap < 8 && pomocnik.getZaszyfrowaneSlowo().contains("_")) {// || - lub , && - też
            pomocnik.wyborGracza();
            System.out.println(pomocnik.getZaszyfrowaneSlowo());

            etap = pomocnik.getEtap();
        }
        if (!pomocnik.getZaszyfrowaneSlowo().contains("_")) {
            System.out.println("Gratuluje, wygrałeś!");
            System.out.println("Słowo to  " + pomocnik.getZaszyfrowaneSlowo());
        }
    }
}

class pomocnikWisielec {
    private String losoweSlowo;
    private int etap = 1;
    private List<String> litery = new ArrayList<>();
    private String zaszyfrowaneSlowo = "";
    private String[] slowa = {"konwalie", "sosna", "obiekty", "klasy"};

    public int getEtap() {// nie jest dostępny bo jest prywatny, żeby się dostać muszę go pobrać
        return etap;
    }


    public String getZaszyfrowaneSlowo() {
        return zaszyfrowaneSlowo;
    }

    public void przywitanie() {
        System.out.println("Witaj w grze wisielec, twoim zadaniem jest odgadnąć słowo, powodzenia!");
    }

    public String setLosoweSlowo() {
        int index = (int) (Math.random() * slowa.length);
        losoweSlowo = slowa[index];
        return losoweSlowo;
    }

    public void szyfrowanie() {
        zaszyfrowaneSlowo = "";
        String[] podzieloneSlowo = losoweSlowo.split("");
        for (String letter : podzieloneSlowo) { // każdy kolejny element w tablicy podzielone słowo w tym przypadku litera bo słowo mamy podzielone na litery
            if (litery.contains(letter)) {
                zaszyfrowaneSlowo = zaszyfrowaneSlowo + letter;
            } else {
                zaszyfrowaneSlowo = zaszyfrowaneSlowo + "_";
            }
        }
    }

    public void wyborGracza() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz litere");
        String wybor = sc.nextLine();

        if (losoweSlowo.contains(wybor)) {
            litery.add(wybor);
            szyfrowanie();
        } else {
            szubienica(etap++);
        }
    }


    public void szubienica(int etap) {

        if (etap == 1) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println("___|___");
        }
        if (etap == 2) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("___|___");
        }
        if (etap == 3) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println("   ____________");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   | ");
            System.out.println("___|___");
        }
        if (etap == 4) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("___|___");
            ;
        }
        if (etap == 5) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |           |");
            System.out.println("   |           |");
            System.out.println("   |");
            System.out.println("___|___");
            ;
        }
        if (etap == 6) {
            System.out.println("Zle, sprobuj ponownie");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |           |");
            System.out.println("   |           |");
            System.out.println("   |          / \\ ");
            System.out.println("___|___      /   \\");
        }
        if (etap == 7) {
            System.out.println("KONIEC GRY!");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |          _|_");
            System.out.println("   |         / | \\");
            System.out.println("   |          / \\ ");
            System.out.println("___|___      /   \\");
            System.out.println("Koniec gry!");
        }
    }
}

